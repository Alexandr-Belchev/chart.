import truncateFunc from './scripts/truncate'
const swup = new Swup()


let createChart = () => {
  class ChartItem {
    constructor(data, item) {
      this.data = data
      this.item = item
    }
    createSvg() {
      return new Promise((resolve, reject) => {
        let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg")
        let heightAttr = this.item.getAttribute('data-height')
        let collWidth = this.item.getAttribute('data-coll-width')
        let offsetAttr = this.item.getAttribute('data-offset')
        svg.setAttribute("height", heightAttr)
        let svgNS = svg.namespaceURI
        let height = svg.getAttribute('height')
        this.item.appendChild(svg)
        let offset = 0
        this.data.forEach((el, i) => {
          let svgG = document.createElementNS(svgNS, 'g')
          let svgRect = document.createElementNS(svgNS, 'rect')
          svgG.setAttribute('transform', `scale(1,-1) translate(${offset+=parseInt(offsetAttr)}, -${heightAttr})`)
          svgRect.setAttribute('width', `${collWidth}px`)
          svgRect.setAttribute('fill', el.color)
          svgG.appendChild(svgRect)
          svg.appendChild(svgG)
        })
        resolve(this.generateRectsHeight(height))
        reject(new Error('Chart is not built'))
      })
    }
    generateRectsHeight(height) {
      let coefs = new Set()
      this.data.forEach((el, i) => {
        if (height < el.value) {
          coefs.add(el.value / height)
        }
      })
      let coef = Math.max(...coefs.keys())
      let rects = this.item.querySelectorAll('rect')
      this.data.forEach((el, i) => {
        rects.forEach((rect, ind) => {
          if (i == ind) {
            rect.setAttribute('height', el.value / coef + 'px')
          }
        })
      })
    }
  }
  let arr = [{
      value: 242,
      color: '#151'
    },
    {
      value: 24,
      color: '#000'
    },
    {
      value: 140,
      color: '#f05'
    },
    {
      value: 140,
      color: '#f05'
    },
    {
      value: 140,
      color: '#f05'
    },
    {
      value: 107,
      color: '#40f'
    },
    {
      value: 107,
      color: '#67f'
    },
    {
      value: 160,
      color: '#10f'
    }
  ]
  let arr2 = [{
      value: 422,
      color: '#151'
    },
    {
      value: 214,
      color: '#000'
    },
    {
      value: 1340,
      color: '#f05'
    },
    {
      value: 1410,
      color: '#f05'
    },
    {
      value: 1470,
      color: '#f05'
    },
    {
      value: 1107,
      color: '#40f'
    },
    {
      value: 1337,
      color: '#67f'
    },
    {
      value: 160,
      color: '#10f'
    }
  ]
  let chartItems = [
    new ChartItem(arr, document.querySelector('.js-chart')),
    new ChartItem(arr2, document.querySelector('.js-chart2'))
  ]
  chartItems.forEach(el => {
    return new Promise((resolve, reject) => {
      if (el.item) {
        resolve(el.createSvg())
      } else {
        reject(new Error('Element is not exist'))
      }
    })
  })

  let ctx = document.getElementById('chart')

  Chart.defaults.global.defaultFontFamily = "Montserrat"
  Chart.defaults.global.defaultFontSize = 12
  if (ctx) {
    new Chart(ctx, {
      type: 'line',
      data: {
        labels: ['Sveta', 'Alina', 'Valerie', 'Maria', 'Serega', 'Tavryha'],
        datasets: [{
          label: '# of age',
          data: [20, 19, 16, 20, 30, 3],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        title: {
          display: true,
          text: 'Girls chart ? (and Serega with tavryha)',
          position: 'top'
        },
        scales: {
          yAxes: [{
            ticks: {
              callback: (tick) => {
                return `${tick})`
              }
            }
          }]
        }
      }
    })
  }
}
createChart()
truncateFunc()
swup.on('pageView', function () {
  createChart()
  truncateFunc()
})
