$.fn.truncate = function () {
  this.each((i, el) => {
    let chars = el.getAttribute('data-chars')
    $(el).text(`${$(el).text().substr(0, chars)}...`)
    return el
  })
}
export default function truncateFunc() {
  $('.truncate').each((i, el) => {
    $(el).truncate()
  })
}