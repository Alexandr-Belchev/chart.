import $ from 'jquery'
import Swup from 'swup'
import Chart from 'chart.js'

window.Chart = Chart
window.Swup = Swup
window.$ = $
window.jQuery = $
