from django.conf import settings
from django.urls import path, re_path
from django.contrib import admin
from django.conf.urls import url, include
from django.contrib.staticfiles.urls import static
from django.views.generic import TemplateView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
]

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [url(r'^rosetta/', include('rosetta.urls'))]

if settings.DEBUG:
    urlpatterns += [url(r'^', include('markup.urls'))]
    urlpatterns += (
        static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) +
        static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    )

    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar
        urlpatterns += [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ]

urlpatterns += [
    re_path(r'^$', TemplateView.as_view(template_name="index.jinja")),
    re_path(r'^chart/', TemplateView.as_view(template_name="chart.jinja")),
]