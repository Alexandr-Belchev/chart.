webpackJsonp([0],{

/***/ 158:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(159);


/***/ }),

/***/ 159:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _truncate = __webpack_require__(160);

var _truncate2 = _interopRequireDefault(_truncate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var swup = new Swup();

var createChart = function createChart() {
  var ChartItem = function () {
    function ChartItem(data, item) {
      _classCallCheck(this, ChartItem);

      this.data = data;
      this.item = item;
    }

    _createClass(ChartItem, [{
      key: "createSvg",
      value: function createSvg() {
        var _this = this;

        return new Promise(function (resolve, reject) {
          var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
          var heightAttr = _this.item.getAttribute('data-height');
          var collWidth = _this.item.getAttribute('data-coll-width');
          var offsetAttr = _this.item.getAttribute('data-offset');
          svg.setAttribute("height", heightAttr);
          var svgNS = svg.namespaceURI;
          var height = svg.getAttribute('height');
          _this.item.appendChild(svg);
          var offset = 0;
          _this.data.forEach(function (el, i) {
            var svgG = document.createElementNS(svgNS, 'g');
            var svgRect = document.createElementNS(svgNS, 'rect');
            svgG.setAttribute('transform', "scale(1,-1) translate(" + (offset += parseInt(offsetAttr)) + ", -" + heightAttr + ")");
            svgRect.setAttribute('width', collWidth + "px");
            svgRect.setAttribute('fill', el.color);
            svgG.appendChild(svgRect);
            svg.appendChild(svgG);
          });
          resolve(_this.generateRectsHeight(height));
          reject(new Error('Chart is not built'));
        });
      }
    }, {
      key: "generateRectsHeight",
      value: function generateRectsHeight(height) {
        var coefs = new Set();
        this.data.forEach(function (el, i) {
          if (height < el.value) {
            coefs.add(el.value / height);
          }
        });
        var coef = Math.max.apply(Math, _toConsumableArray(coefs.keys()));
        var rects = this.item.querySelectorAll('rect');
        this.data.forEach(function (el, i) {
          rects.forEach(function (rect, ind) {
            if (i == ind) {
              rect.setAttribute('height', el.value / coef + 'px');
            }
          });
        });
      }
    }]);

    return ChartItem;
  }();

  var arr = [{
    value: 242,
    color: '#151'
  }, {
    value: 24,
    color: '#000'
  }, {
    value: 140,
    color: '#f05'
  }, {
    value: 140,
    color: '#f05'
  }, {
    value: 140,
    color: '#f05'
  }, {
    value: 107,
    color: '#40f'
  }, {
    value: 107,
    color: '#67f'
  }, {
    value: 160,
    color: '#10f'
  }];
  var arr2 = [{
    value: 422,
    color: '#151'
  }, {
    value: 214,
    color: '#000'
  }, {
    value: 1340,
    color: '#f05'
  }, {
    value: 1410,
    color: '#f05'
  }, {
    value: 1470,
    color: '#f05'
  }, {
    value: 1107,
    color: '#40f'
  }, {
    value: 1337,
    color: '#67f'
  }, {
    value: 160,
    color: '#10f'
  }];
  var chartItems = [new ChartItem(arr, document.querySelector('.js-chart')), new ChartItem(arr2, document.querySelector('.js-chart2'))];
  chartItems.forEach(function (el) {
    return new Promise(function (resolve, reject) {
      if (el.item) {
        resolve(el.createSvg());
      } else {
        reject(new Error('Element is not exist'));
      }
    });
  });

  var ctx = document.getElementById('chart');

  Chart.defaults.global.defaultFontFamily = "Montserrat";
  Chart.defaults.global.defaultFontSize = 12;
  if (ctx) {
    new Chart(ctx, {
      type: 'line',
      data: {
        labels: ['Sveta', 'Alina', 'Valerie', 'Maria', 'Serega', 'Tavryha'],
        datasets: [{
          label: '# of age',
          data: [20, 19, 16, 20, 30, 3],
          backgroundColor: ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(255, 206, 86, 0.2)', 'rgba(75, 192, 192, 0.2)', 'rgba(153, 102, 255, 0.2)', 'rgba(255, 159, 64, 0.2)'],
          borderColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)', 'rgba(255, 159, 64, 1)'],
          borderWidth: 1
        }]
      },
      options: {
        title: {
          display: true,
          text: 'Girls chart ? (and Serega with tavryha)',
          position: 'top'
        },
        scales: {
          yAxes: [{
            ticks: {
              callback: function callback(tick) {
                return tick + ")";
              }
            }
          }]
        }
      }
    });
  }
};
createChart();
(0, _truncate2.default)();
swup.on('pageView', function () {
  createChart();
  (0, _truncate2.default)();
});

/***/ }),

/***/ 160:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = truncateFunc;
$.fn.truncate = function () {
  this.each(function (i, el) {
    var chars = el.getAttribute('data-chars');
    $(el).text($(el).text().substr(0, chars) + '...');
    return el;
  });
};
function truncateFunc() {
  $('.truncate').each(function (i, el) {
    $(el).truncate();
  });
}

/***/ })

},[158]);
//# sourceMappingURL=main.js.map